---
date: 2018-03-02
title: "Atelier CNV - l'émotion chez soi et chez les autres"
duration: 110 min
genre: Atelier
authors:
  - quille_julie
tags:
  - communication
  - émotions
  - besoins
  - conscience
  - moi et l'autre
illustration:
  name: giraffe.jpg
  source: https://pixabay.com/en/giraffe-animal-africa-sunset-2073609/
abstract: |
  Je vous propose ici un atelier de deux heures pour cheminer au coeur d'une relation entre l'émotion de l'autre et la notre. Au court de ce voyage nous ferons des points d'étapes pour comprendre le concept de souveraineté, comment nous pouvons sortir d'une situation émotionnelle perdant-perdant ou encore le lien entre les émotions et les besoins. L'objectif de tout cela ? D'abord et avant tout, nous donner une vision différente des choses et nous permettre d'améliorer notre quotidien.
sessions:
  - date: "2018-11-08"
    name: Agile Tour Lille 2018
    url: http://2018.agiletour-lille.org/
ressources:
  - name: "Le support de présentation Agile Tour Lille 2018"
    file: 2018-11-08_emotions_travail.pdf
---

# Présentation détaillée

Cette session de deux heures est née d'une situation en entreprise où je me suis retrouvée avec une équipe entière en situation de stress émotionnel, avec chacun qui s'enfonçait un peu plus dans une cacophonie émotionnelle dans laquelle nous ne pouvions plus voir grand chose. L'idée : prendre du recul, respirer, se poser et détricoter tout ça fils après fils.
Ce sont donc deux heures de cheminement au coeur d'une relation entre l'émotion de l'autre et la notre que je vous propose. Au court de ce voyage nous ferons des points d'étapes pour comprendre le concept de souveraineté, comment nous pouvons sortir d'une situation émotionnelle perdant-perdant ou encore le lien entre les émotions et les besoins. L'objectif de tout cela ? D'abord et avant tout, nous donner une vision différente des choses et nous permettre d'améliorer notre quotidien.

# Objectifs de la session

- Partager avec vous une facette de la CNV
- Vous faire vivre un moment de relation à vous
- Travailler la souveraineté
- Comprendre un des aspects systèmique de l'émotion

# Structure de la session

En groupe de 10 personnes maximum, je vais vous accompagner à travers un cas pratique à l'aide d'une situation, de cartes, d'explications et, pour ceux qui le souhaitent, de partages.

# Tweet

Un atelier, des émotions, des besoins, un voyage au centre de nous-même pour initier une réflexion sur notre rapport à nous et à l'autre. Une aventure sur mesure pour les plus courageux !

Une aventure sur mesure dans les montagnes russes de l'émotionnel et des besoins : pour les plus audacieux seulement !
