---
date: 2020-02-24
title: "Programmation multi-agents pour les nuls"
duration: 2h
genre: Atelier
authors:
  - clavier_thomas
tags:
  - programmation
  - technique
  - atelier
  - post-it
illustration:
  name: agents.jpg
abstract: |
 À quoi ressemblent la programmation multi-agents, et la programmation répartie ? Quelles sont les différences avec une architecture micro-services ?
  Pendant 2h, nous vous proposerons de venir incarner un programme multi-agents. Soyez l'un des agents Smith, puis prenez du recul pour sortir de la matrice.
---

# Présentation détaillée

Cet atelier va vous plonger au milieu de la programmation multi-agents. L'idée est de comprendre les principales contraintes de la programmation multi-agents de l'intérieur. Après avoir incarné l'agent Smith nous vous inviterons à sortir de la matrice pour mieux comprendre les compromis à faire.

Programmeurs confirmés ou débutants, en ressortiront riches d'une nouvelle expérience à reproduire dans leurs codes. À ne pas louper si vous êtes en recherche de perfectionnement:une expérience ludique et amusante pour apprendre un concept complexe et rarement utilisé.

# Objectifs de la session

- comprendre les enjeux de la programmation multi-agents
- vivre les écueils de la programmation multi-agents sans prendre de risque 
- apprendre en s'amusant

# Structure de la session

Pendant cette expérience partagée de deux heures autour d'un kata très simple : résoudre un sudoku. Chaque personne va progressivement prendre la responsabilité de résoudre une partie du problème tout en observant le fonctionnement du groupe. Nous aurons ainsi une vision systémique et ludique du groupe ce qui permettra de changer les agents avant de repartir sur une autre itération d'apprentissage.

En fin de session, nous prendrons le temps de résumer et partager nos différents apprentissages.

# Tweet

Comment programmer un système multi-agents en 2h ? Avec des post-it !
