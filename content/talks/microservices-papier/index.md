---
date: 2018-01-20
title: "Mon microservice en papier"
duration: 2H
draft: true
genre: Atelier
authors:
  - clavier_thomas
tags:
  - craft
  - software craftsmanship
  - microservice
  - système réparti
illustration:
  name: measure.jpg
  source: https://flic.kr/p/5MbgMi
abstract: |
  Découverte des avantages et des inconvénients d'une architecture micro-services et réparti à travers une expérience grandeur nature ou chaque binôme incarnera un micro-service. Cet atelier, ouvert à tous, a été à l'origine conçu pour des gens travaillants dans l'informatique. Il permet de se rendre compte par l'expérience du compromis réalisé lorsque l'on choisit une architecture micro-services. Il met le doit sur les difficultés des systèmes répartis, mais aussi sur les avantages d'une telle architecture.

---

# Objectifs de le session

Trop souvent confronté à des équipes convaincu que le tout micro-services est la solution à tout, ou au contraire que le tout monolithique est l'unique solution, j'ai monté cet atelier dans le but de faire vivre aux équipes une expérience qui fait réfléchir sans avoir besoin de coder pendant des heures avant d'être confronté aux problèmes.


# Contraintes

Un espace suffisemment grand pour que chaque micro-service soit sur une table avec de l'espace entre les tables pour assurer des déplacements entre micro-service.



