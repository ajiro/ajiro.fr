---
date: 2018-01-20
title: "Host leadership, la métaphore de l'hôte au service du leadership"
duration: 1H
draft: false
genre: Présentation
authors:
  - clavier_thomas
  - benoist_alexis
tags:
  - leadership
  - management
illustration:
  name: wedding.jpg
ressources:
  - name: "Support de présentation"
    file: https://azae.gitlab.io/conferences/host-leadership/
  - name: "Host leadership board"
    file: https://azae.gitlab.io/conferences/host-leadership/host-leadership-board.pdf
  - name: "Sources de la présentation"
    file: https://gitlab.com/azae/conferences/host-leadership
sessions:
  - date: "2019-06-27"
    name: Agile Tour Laval 2019
    url: https://www.agilelaval.org/
  - date: "2019-10-30"
    name: Agile Tour Nantes 2019
    url: http://www.agilenantes.org/agile-tour-nantes-2019/
abstract: |
  Hôte ou hôtesse, personne qui donne l’hospitalité ... Et si l'analogie de l'hôte nous donnait les moyens d'êtres tous leaders ? D'ailleurs, n'avons-nous jamais été leader en organisant une fête ou une soirée ? Cette conférence atelier vous emportera dans une expérience nouvelle du leadership. Vous en repartirez avec un outil riche et puissant capable de révéler le leader qui sommeille en vous.

  Cette session est ouverte à tous, et aucune compétence particulière n'est nécessaire pour l'apprécier à sa juste valeur.

---

# Objectifs de le session

À travers cette session mon objectif est d'apporter confiance en soit et leadership à l'auditoire. Ils pourront repartir avec un outil riche et puissant capable de les épauler dans leurs activités. Faire découvrir la métaphore de l'hôte est pour moi l'occasion de montrer à chacun que tout le monde est capable d'être leader, que tout le monde a déjà été leader. C'est aussi un outil extraordinaire pour améliorer son leadership tout en acceptant ses faiblesses.

Cette session à été joué de nombreuses fois, en entreprise et en conférence.

# Pitch

Voyez d'une nouvelle manière le leadership avec le Host Leadership
