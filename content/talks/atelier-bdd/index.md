---
date: 2018-10-17
title: "Vivre le Behavior Driven Development (BDD)"
duration: 2H
genre: Atelier
draft: false
authors:
  - clavier_thomas
tags:
  - Software Craftsmanship
  - bdd
  - concepts
  - atelier
  - conference
illustration:
  name: craftsmanship.jpg
abstract: |
  Cet atelier présente le Behavior Driven Development (BDD) et propose d'apprendre en faisant. À travers un cas concret vous pratiquerez la conception et la rédaction collaborative de scénarios BDD.
  Cet atelier donnera l'envie d’approfondir le sujet afin de généraliser l'écriture de tests d'acceptance de haute qualité ou plus précisément, de "spécifications exécutables", pour écrire un code mieux conçu, plus facile à maintenir et plus fiable.

  Pas la peine de savoir coder ! L'atelier est tourné sur la discussion, la construction de scénarios et la rédaction de spécifications par l'exemple.
  Chers gens du métier, manager ou curieux, venez découvrir le monde du BDD.
---

## Disposition de la pièce

Une grande salle avec des ilots de 6 personnes.

