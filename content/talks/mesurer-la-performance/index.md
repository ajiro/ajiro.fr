---
date: 2018-01-20
title: "Indiana Jones et le temple des indicateurs"
duration: 1H
draft: false
genre: Présentation
authors:
  - clavier_thomas
tags:
  - craft
  - kpi
  - management
  - software craftsmanship
illustration:
  name: indiana-jones-lego.jpg
  source: https://flic.kr/p/4JLN4i
abstract: |
  Le temple du bon indicateur, bien caché au fond d'une jungle d'OKR, de KPI et autres objectifs annuels, semble bien inaccessible. Pourtant il doit bien être possible de mesurer la performance d'une personne ? Et si ce n'était pas possible ? Essayons ensemble d'apprendre des erreurs des autres. Pour ce faire nous allons étudier quelques cas d'indicateurs spécialement inefficaces avant d'étudier des cas ayant très bien fonctionné.

  Managers, venez comprendre comment faire pour arrêter de mesurer n'importe quoi ! Développeurs venez trouvez quelques pistes pour éduquer vos managers. Et si après cette conférence vous étiez capable de mesurer objectivement le monde qui nous entoure ?
---

# Objectifs de le session

Régulièrement confronté à des organisations qui cherchent à mesurer l'efficacité des développeurs, je souhaite avec cette conférence apporter plusieurs choses :

- pour les manageurs, une prise de conscience sur l'inutilité des indicateurs topdown
- pour les développeurs, un ensemble d'outils et d'histoires pour répondre aux indicateurs topdown sans effort
- pour les coachs agiles, souligner les gros risques des matrices de maturité
- pour l'ensemble de l'auditoire, passer un bon moment ensemble à rigoler de certaines anecdotes.



# Structure de la session

Loi de Campbell

