---
date: 2019-08-31
title: "Elephant Carpaccio"
duration: 1H
draft: true
genre: Atelier
authors:
  - benoist_alexis
tags:
  - leadership
  - management
illustration:
  name: TODO.jpg
  source: https://flic.kr/p/5MbgMi
abstract: |
  Marre des projets qui n'avancent pas ?  Venez découvrir comment découper des features en micro-incréments pour livrer de la valeur tous les jours pour les clients! Dans cet atelier, on va expérimenter sur un véritable cas d'utilisation.
---

# Objectifs de le session

Le but de cet session est d'expérimenter et de partager sur le slicing.


# Pitch

Venez découvrir comment manger un éléphant tout entier !
