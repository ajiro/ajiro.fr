---
date: 2019-09-16
title: "Vous saurez tout, tout, sur le BDD !"
duration: 1H
genre: Présentation
draft: false
authors:
  - clavier_thomas
tags:
  - Software Craftsmanship
  - bdd
  - concepts
  - conference
illustration:
  name: craftsmanship.jpg
abstract: |
  Cette conférence présente le Behavior Driven Development (BDD) et propose d'apprendre en faisant.
  Cette conférence donnera l'envie d’approfondir le sujet afin de généraliser l'écriture de tests d'acceptance de haute qualité ou plus précisément, de "spécifications exécutables", pour écrire un code mieux conçu, plus facile à maintenir et plus fiable.

  Pas la peine de savoir coder ! La conférence est tourné sur la discussion, la construction de scénarios et la rédaction de spécifications par l'exemple.
  Chers gens du métier, manager ou curieux, venez découvrir le monde du BDD.
ressources:
  - name: "Slides de la présentation"
    file: https://azae.gitlab.io/conferences/behavior-driven-development/

---

## Description

Cette session d'une heure déroulera les étapes suivantes :

* Le point de départ, l’échange entre les 3 amis.
* Seconde étape, c’est la création d’un langage commun : “l’Ubiquitous Language”
* Troisième étape, l’utilisation d’outils complémentaires pour minimiser les incompréhensions. Les exemples et les personas.
* Enfin, dernier point, le langage Gherkin et l’automatisation.
* Petit aparté avant de terminer avec un zoom sur la structure d’un bon exemple.
* En guise de conclusion, ne pas confondre UI (Interface Utilisateur) et UX (Expérience Utilisateur)

Pas la peine de savoir coder ! Pas la peine d'être PO ou développeur pour comprendre ce qu'est le BDD. Cette session se veut pédagogique et didactique pour comprendre les tenants et les aboutissants du BDD.

Chers gens du métier, manager, développeur, testeurs ou curieux, venez découvrir le monde du BDD.

Découvrir quelques bonnes pratiques du génie logiciel : TDD, Clean Code, Refactoring, Dette Technique et Integration Continue sont au programme !

## Disposition de la pièce

Un amphi, une grande salle.

# Tweet

Vous saurez tout, tout, sur le BDD !
