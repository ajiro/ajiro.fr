---
date: 2021-03-29
sorted:
title: "Paradigmes CNV"
draft : false
categories:
- cnv
tags:
- atelier
- CNV
- outils
- paradigmes
objectives: |
  - présentation des paradigmes en CNV
  - avoir un visuel récapitulatif
  - revoir, compléter, co-construire les paradigmes
authors:
  - quille_julie
illustration:
  name: besoins.png
ressources:
  - name: Supports paradigmes
    file: 2021-03_Paradigmes.pdf
    icon: fr.svg

---

# Contenu

- Schémas des paradigmes "Manque" / "Abondance" ou "Chacal" / "Girafe" à completer
- étiquettes pour compléter les schémas
- formats A2 / A3 / A4

# Principes des schémas

- Les schémas reprennent les idées clés des paradigmes avec les croyances et leurs conséquences
- L'idée est qu'ils servent de support à l'échange et à la réflexion et non pas qu'ils servent à chercher "les bonnes réponses"
- Vous pouvez compléter avec ce qui vous semble essentiel, utiliser les mots qui vous parlent etc.

# Proposition d'utilisation

1. Proposer l'activité en individuel ou en groupe,
2. Distribuer les deux schémas vide ainsi que les étiquettes,
3. Présenter les deux schémas, vous pouvez par exemple compléter tous ensemble la première case "croyance" (jaune)
4. À partir de là vous pouvez choisir de guider la refléxion ou de laisser en autonomie. Dans le cas où vous guidez vous pouvez poser des questions telles que :
    - Comment vous vous sentez à l'idée que tout le monde a des besoins et qu'il n'y a pas de quoi répondre aux besoins de tout le monde ?
    - Dans ce cas comment vous voyez les autres ?
    - Si nous pensons que les autres ne sont pas enclins à contribuer à nos besoins, qu'est-ce que nous pouvons faire pour qu'ils s'en occupent quand même ? etc.
5. Prenez le temps de debriefer, d'accueillir les réactions, désaccords qui peuvent émerger avec la conscience que ces schémas sont eux-mêmes une façon de simplifier le monde et que chacun·e a la liberté de ne pas adhérer à cette façon de modèliser la CNV.
