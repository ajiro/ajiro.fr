---
date: 2019-01-08
sorted:
title: "Appréhender les émotions fortes"
draft : false
categories:
- cnv
tags:
- émotions
- besoins
- atelier
- CNV
- responsabilisation individuelle
objectives: |
  - comprendre la relation entre sentiments et besoins
  - expérimenter sur un cas concret
  - être mieux armé pour appréhender les émotions
duration: 1 à 2 heures
participants: "Une dizaine de personnes par animateur maximum"
materials:
  - supports avec la liste des émotions et des besoins (cartes émotions-besoins, ou listes papier)
  - post-it
authors:
  - quille_julie
illustration:
  name: agile-tour-lille-2.jpeg
references:
  - title: Présentation de l'atelier
    icon: external-link
    url: /talks/atelier_cnv/

---

# Présentation

Cet atelier, joué de nombreuses fois en entreprise, en off d'Agiles Tours et même inscrit au programme de l'Agile Tour Lille 2018, est une excellente façon d'appréhender le fonctionnement des émotions : leur impact, leur raison d'être, leur unicité. C'est également l'occasion d'explorer d'autres manières de faire que celle dont nous avons l'habitude, pour, qui sait, peut-être augmenter notre capacité individuelle à vivre quotidiennement aux côtés de ces émotions, pas toujours agréables.

# Déroulé

## Une situation

Trouver une situation :
Demander à chaque participant de se remémorer une situation dans laquelle ils ont été confrontés à l'expression forte d'une émotion désagréable (colère, tristesse etc.).
Cette situation sera notre situation de base pour l'ensemble de l'atelier.
Préciser qu'à aucun moment il ne sera demandé de parler aux autres de cette situation.

Noter sur un petit post-it, le prénom de la personne, "Elle", "Lui" ou "L'autre"

## Son sentiment

<img src="sen-bes-4.png" class="img-fluid" alt="Sous titres"/>

1. Demander aux participants d'imaginer ce que l'autre personne a ressenti (trouver l'émotion, le sentiment, le ressenti de la personne qui a exprimé une émotion désagréable).
À cette étape vous pouvez donner les supports avec la liste des émotions aux participants.
Lorsqu'ils l'ont trouvé, ils inscrivent cette émotion sur un post-it orange.
Il arrive régulièrement qu'il y ait plus d'un sentiment : à vous d'accompagner les participants pour qu'ils essayent de faire le tri, et d'accepter que parfois on préfère en garder plusieurs, ce qui n'est pas problématique.
3. Faire un tour de table où chacun va énnoncer le ou les sentiments qu'il a imaginé(s).
L'idée est seulement de préciser le sentiment, l'explication de la situation n'est pas nécessaire.
3. Petit point théorique : vous pouvez faire remparquer que nous sommes dans une supposition de ce que l'autre ressent. L'idée est d'essayer de comprendre ce qui se passe, sans pour autant en être certain, et c'est tout à fait normal d'imaginer.

## Mon sentiment

<img src="sen-bes-3.png" class="img-fluid" alt="Sous titres"/>

1. Demander à chacun de noter "Moi" sur un petit post-it
4. Demander maintenant à chacun de trouver comment lui-même s'est senti face à l'expression de ce sentiment. Soyez là encore attentif au fait que nous réagissons par rapport au sentiment et non par rapport à la situation. Utiliser le même support pour les émotions que précédemment. Écrire cette émotion sur un autre post-it orange.
5. Faire un tour de table pour que chacun puisse exprimer comment il s'est senti face à l'expression forte d'un sentiment négatif de la part de quelqu'un d'autre.
6. Petit point théorique : faire remarquer qu'en général on éprouve soit même un sentiment désagréable face à l'expression d'autrui d'un sentiment désagréable. Il est important de souligner que nous pourrions être serein, en empathie, touché etc. face à cette émotion et que notre réaction nous appartient. Il est également important de souligner l'effet boule de neige que peut provoquer cette réaction : "il est en colère", du coup "je suis apeuré", donc "il va se sentir coupable", ce qui fait que "je vais me sentir mal à l'aise" etc. Dans ces cas là, il est important de voir d'abord comment nous pouvons gérer notre émotion, et si nous sommes en mesure ou non de nous occuper de celle de l'autre.


## Son besoin

<img src="sen-bes-2.png" class="img-fluid" alt="Sous titres"/>

7. Petit point théorique : expliquer le lien entre émotion et besoin (l'émotion est un voyant qui indique des besoins satisfaits (émotions généralement agréables) ou insatisfaits (émotions généralement désagréables)
8. Demander à chacun d'imaginer quel est le besoin derrière l'émotion de la personne dans la situation choisie. C'est le moment de fournir les supports besoins. Écrire le besoin identifié sur un post-it vert. Là encore vous allez être amené à aider les participants à faire le tri.
9. Faire un tour de table pour que chacun redonne le sentiment identifié pour l'autre, ainsi que le besoin qu'il imagine derrière.

## Mon besoin

<img src="sen-bes-1.png" class="img-fluid" alt="Sous titres"/>

10. Demander aux participants d'imaginer le besoin présent derrière leur propre sentiment. Inscrire ce besoin sur un autre post-it vert.
11. Petit point théorique : vérifier qu'avec le besoin nous essayons bien de comprendre l'émotion et non pas de la faire disparaitre. Par exemple : j'ai besoin de repos parce que je suis en colère (besoin de repos = stratégie et non pas besoin). Pour vérifier demander de faire passer les deux relations sentiment-besoin dans les phrases tests suivantes :
  - Je me sens "sentiment", parce que j'ai besoin de "besoin". (relation ok)
  - J'ai besoin de "besoin", parce que je me sens "sentiment". (relation inversée)
12. Faire un tour de table pour que chacun puisse exprimer le besoin qu'il identifie derrière son propre sentiment. S'il y a un doute vérifier que la condition du point précédent est bien respectée.

## Mon sentiment face à son besoin

<img src="sen-bes-5.png" class="img-fluid" alt="Sous titres"/>

13. Demander aux participants de déplacer le post-it vert "besoin de l'autre", sur le post-it orange "sentiment de l'autre", de manière à masquer ce dernier.
14. Demander aux participants de voir si leur émotion est toujours la même lorsqu'ils regardent le besoin de l'autre plutôt que le sentiment.
15. Faire un tour de table pour que chacun puisse exprimer si cela a changé quelque chose pour lui ou non. Habituellement le sentiment évolue vers quelque chose de plus agréable.
Plusieurs cas peuvent empêcher l'apaisement de l'émotion désagréable que nous ressentons, voici quelques possibilités :
  - Nous ressentons le besoin de l'autre comme allant à l'encontre de nos propres besoins
  - Nous nous sentons responsables du besoin de l'autre
  - Il y a un passif trop important avec la personne pour pouvoir entrer en empathie avec elle.
Chacun fait en fonction de ce qui est bon pour lui, il n'y a pas de bonne ou de mauvaise réaction.
16. Idéalement garder un petit temps en commun pour débriefer.

# Variante

## Atelier commun à une équipe

Il est possible de dérouler cet atelier suite à une situation ayant généré de l'incompréhension et qui se focalise sur un membre de l'équipe. Dans ce cas là, pour la situation de départ, rappeler les faits (sans nommer d'émotion), et donner le nom de la personne dont on va essayer de comprendre les émotions et besoins. Nous partons du principe que la personne concernée n'est pas présente. Dans le cas contraire, certains ajustements sont nécessaires.
Dans tous les cas on sera attentif à l'intention de l'atelier qui est d'augmenter la compréhension et de comprendre la réaction de chacun vis à vis d'une même situation. A aucun moment la personne clé ne doit être stigmatisée.

## Matériel
Pour le matériel il est possible d'utiliser une liste d'émotions et une liste de besoins, ou des cartes. Préférer la première dans les grands groupes ou lorsque le temps est contraint. Le second permettra des échanges différents, à utiliser notamment dans des petits groupes (une dizaine).

# Bilan

Cet atelier est avant tout une expérience servant à favoriser les prises de conscience et à faire des apprentissages. Si vous souhaitez apaiser les personnes en recherche de solutions concrètes il est possible de présenter quelques outils qui permettent d'introduire les émotions et la gestion des émotions dans un cadre professionnel.
