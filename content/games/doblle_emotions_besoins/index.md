---
date: 2021-03-26
sorted:
title: "Dobble des émotions et des besoins"
draft : false
categories:
- cnv
tags:
- atelier
- CNV
- cartes
- outils
- émotions
- besoins
objectives: |
  - permettre de travailler les besoins et les sentiments
  - soutenir les consignes et objectifs de l'atelier proposé
authors:
  - quille_julie
illustration:
  name: hands.jpg
ressources:
  - name: Fichier de cartes sans fond
    file: Dobble_emotions_besoins.pdf
    icon: fr.svg
  - name: Fichier de cartes avec fond
    file: Dobble_emotions_besoins_fond.pdf
    icon: fr.svg

---

# Principes des cartes

- Chaque carte est unique
- Chaque carte émotion et chaque carte besoin à 1 et un seul besoin ou sentiment en commun avec chaque autre carte besoin / sentiment
- Distribuer une carte par personne
- Les cartes ·besoin· et les cartes émotion sont indépendantes

# Proposition d'utilisation

1. Distribuer une carte à chacun·e
2. Créer des binômes
3. Demander au binôme de trouver le besoin / sentiment commun et discuter autour de ce besoin / sentiment

# Idées de questions autour des sentiments et des besoins

- Est-ce que c’est un S / B avec lequel vous êtes familier ?
- Est-ce que vous vous raconter des histoires sur ce S / B ?
- Comment / Quand est-ce qu’il se manifeste chez vous ? Chez les autres ?
- Etc.
