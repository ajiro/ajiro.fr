---
date: 2020-04-15
sorted:
title: "Jeux autour de l'observation"
draft : true
categories:
- cnv
tags:
- observation
- atelier
- CNV
objectives: |
  - travailler l'observation
duration: 30 min environ
participants: "autant qu'on le souhaite"
materials:
  - papier
  - stylos
  - tableau
authors:
  - leguiffant_alice
illustration:
  name: eyes.png


---

# Présentation

Je vous propose ici quelques petits jeux simples et rapide pour aborder la notion d'observation en CNV.

# Déroulé

## Phase d'observation / description

1. Demander à une personne du groupe de sortir avec la consigne de revenir dans la salle quelques minutes plus tard et d'y faire ce qu'elle souhaite sans parler.
2. Les autres participant·e·s reçoivent pour consignes de noter tout ce qu'iels observent.
3. La personne qui était sortie rentre et fait ce qu'elle veut pendant environ 5 minutes, les autres tentent d'écrire ce qu'iels voient.

## Phase de debrief

1. L'animateur·rice sépare le tableau en deux colonnes sans les nommées. Ensuite iel écrit les phrases dans la première colonne si ce sont des observations et dans la seconde si ce sont des interprétations.
2. Après avoir écrit quelques phrases iel interroge les participant·e·s sur le pourquoi des deux colonnes. Poursuivre jusqu'à ce que le groupe trouve.

## Phase d'ancrage

1. Séparer l'espace en trois : observation / zone d'incertitude / interprétation
2. Demander aux participant·e·s de lire les phrases restantes, le groupe se déplace alors dans la salle dans la zone qui lui semble correspondre à la phrase lue.

## Variante

Utiliser la description de la pièce pour faire cet exercice.

# Autre jeu

Commentaire en direct d'un duo dans l'espace, sur le mode ludique du commentaire sportif :

- 1 groupe qui bouge (il peut danser, ou tout autre action...)

- 1 groupe qui commente (alors le personnage en blanc s'approche de la personne qui est assise... »

- 1 groupe qui dit si c'est une observation ou un jugement
