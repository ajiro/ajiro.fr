---
date: 2017-06-02
sorted: delegation poker
title: "Delegation poker"
tags:
  - management 3.0
objectives: |
  - Clarifier les zones de délégations
duration: 10 - 20 min
participants: "L'équipe et son manager"
materials:
  - des cartes de delegation poker
authors:
  - albiez_olivier
illustration:
  name: delegation-poker
  source: http://flic.kr/p/b8AiDT
references:
  - title: Delegation poker original
    icon: external-link
    url: https://management30.com/product/delegation-poker/
  - title: Code source
    icon: external-link
    url: https://gitlab.com/ajiro/publications/tree/master/delegation_poker
ressources:
  - name: Delegation poker (français)
    file: delegation-poker-fr.pdf
    icon: fr.svg
  - name: Delegation poker (english)
    file: delegation-poker-en.pdf
    icon: en.svg
---
