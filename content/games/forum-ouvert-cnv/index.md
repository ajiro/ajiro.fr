---
date: 2021-02-12
sorted:
title: "Aides pour un forum ouvert d'échange et de partage"
draft : false
categories:
- cnv
tags:
- atelier
- CNV
- forum ouvert
- outils
objectives: |
  - donner quelques supports pour soutenir des ateliers dans un forum ouvert
  - source d'inspiration
authors:
  - quille_julie
illustration:
  name: forum-ouvert.jpg
ressources:
  - name: Support d'aide pour clarifier un sujet
    file: 202102_AideClarificationSujet.pdf
    icon: fr.svg
  - name: Support d'aide pour la prise de note
    file: 202102_AidePriseNotes.pdf
    icon: fr.svg
  - name: Support d'aide déroulé atelier partage / présentation
    file: 202102_DérouléPartage.pdf
    icon: fr.svg
  - name: Support d'aide déroulé atelier apprentissage
    file: 202102_DérouléApprentissage.pdf
    icon: fr.svg
  - name: Support d'aide déroulé atelier co-construction
    file: 202102_DérouléCo-construction.pdf
    icon: fr.svg
  - name: Mémo Conversation Café (traduction)
    file: 202103_ConversationCafe.pdf
    icon: fr.svg

---

# Présentation

  Ces documents ont vocation à venir soutenir la participation au Forum Ouvert CNV.

Les particularités :

  - La seule attente est de trouver chacun·e de la valeur à ce que nous faisons
  
  - Apprentissage à suivre ses pieds et créer des espaces qui nous enrichissent

  Je ne proposerais pas les mêmes documents dans un Forum Ouvert où il y a une intention de création de commun, de résolution de problématique précise, de soutien à une intention autre que celle de l'événement lui-même.

# Inspiration du résultat que moi je peux attendre lorsque je fais des sessions

Lorsque je fais un atelier de co-création d'atelier, je cherche à avoir quelque chose qui ressemble à ça :

* https://ajiro.fr/games/frein_ecoute/
* https://ajiro.fr/games/emotions_fortes/

Lorsque je fais un atelier de partage autour d'une question, je cherche à avoir quelque chose qui ressemble à ça :

* https://ajiro.fr/articles/2020-10-06-langage-dynamique/

Envoyez moi vos propres exemples pour vivre davantage d'inspiration !
