---
date: 2022-10-15
sorted:
title: "Flow Game"
draft : false
categories:
- agile
tags:
- agilité
- icebreaker
- atelier
- rétrospective
- amélioration continue
objectives: |
  - Mettre en pratique le travail par itération
  - Cohésion de groupes
  - Mise en mouvement
duration: 25 min environ
participants: "5 à 35 personnes"
materials:
  - 3 balles
  - Paper
  - Feutres
authors:
  - quille_julie
illustration:
  name: flow-game.jpeg

---

# Présentation

Icebreaker permettant de simuler l'amélioration continue à travers 4 à 8 itérations visant à améliorer le temps pour atteindre un objectif prècis.

# Déroulé

## Définir les ordres de passage :

L'animateur annonce :
" Je vais vous faire passer une balle. Elle doit être envoyée à tout le monde une fois ni plus ni moins. Je vous demande également de ne pas la faire passer à votre voisin."
- Envoyer la première balle et attendre qu'elle ait fait le tour en vérifiant que tout le monde la touche une fois
- Faire passer la seconde balle en demandant au groupe de respecter l'ordre de passage du premier tour
- Faire la même chose avec la troisième balle
" Nous venons de définir l'ordre de passage des balles _(rappeler le)_ ainsi que l'ordre de passage des personnes

## Première itération

Écrire votre besoin sur un paper :
- Tout le monde touche chacune des balles
- L'ordre des balles est respecté
- L'ordre des personnes est respecté

Dessiner un tableau à 3 colonnes (N° de l'itération / Estimation / Réalisé) :
- Demander au groupe combien de temps il pense faire pour accomplir la mission
- Inscrire le temps estimé
- Lancer la première itération en chronométrant
- Inscrire le temps réalisé

## Processus itératif

- Demander si le groupe pense pouvoir faire mieux
- Demander qu'est-ce qu'iels comptent faire de différent pour améliorer le temps
- Lancer l'itération suivante

Répéter ce processus entre 3 et 5 fois jusqu'à ce que le groupe commence à stagner.

Attention :
- La consigne de ne pas envoyer la balle au voisin à disparue. Si le groupe ne s'en ait pas rendu compte lui même (en général à la deuxième ou troisième itération), bien rappeler votre besoin, en rappelant que ce sont VOS SEULES contraintes

- Annoncer que les groupes arrivent en général à atteindre l'objectif en moins d'1 seconde
- Les laisser faire quelques itérations supplémentaires jusqu'à ce que le groupe atteigne l'objectif ou qu'il souhaite arrêter (si vous ne savez pas comment faire en moins d'une seconde : contactez-nous ;-) )

## Debrief

Demander au groupe ce qu'il retient de l'expérience
Pensez à montrer :
- auto-organisation : tout le monde propose des idées (enfin en génèral)
- apprentissage par expérimentation
- chercher de l'inspiration quand on se sent à court d'idée
