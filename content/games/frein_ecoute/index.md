---
date: 2020-04-23
sorted:
title: "Les pièges de l'écoute empathique"
draft : false
categories:
- cnv
tags:
- écoute
- atelier
- CNV
- empathie
objectives: |
  - éxpérimenter les différents pièges à l'écoute empathique
  - prendre conscience de nos habitude d'écoute
duration: 15 - 20 min
participants: "jusqu'à 20 pers."
materials:
  - supports avec la liste des "pièges" (A4 avec un "piège" par feuille)
  - feuilles sur lesquelles écrire les pièges
authors:
  - quille_julie
illustration:
  name: piege.jpg
ressources:
  - name: Cartes freins groupe (A4) (français)
    file: freins-grands.pdf
    icon: fr.svg
  - name: Cartes freins individuels (français)
    file: freins-petits.pdf
    icon: fr.svg


---

# Présentation

Atelier ludique dont l'objectif est d'expérimenter les différents types d'écoute, et de non écoute, afin d'identifier les freins à l'écoute empathique telle que définie dans la CNV.

# Déroulé

1. Un personne se mets face au groupe pour raconter un petit désagrément (réel ou inventé). Attention de bien préciser que l'écoute risque de ne pas être très agréable.
2. L'animatreur·rice se met face au groupe, derrière la personne qui raconte, et montre un des papiers sur lesquels un "frein à l'écoute empathique" est écrit.
3. Le groupe intéragit avec l'apprenant·e qui raconte une histoire, en incarnant le frein proposé. Il est possible que le groupe ait des difficultés à incarner le frein, dans ce cas l'animateur·rice peut rejoindre le groupe pour aider à créer l'échange.
4. Une fois l'histoire à peu près terminée, ou l'expérience suffisamment vécue, demander comment la personne qui racontait l'histoire se sent et éventuellement si elle a deviné le frein que le groupe incarnait
5. Demander à une autre personne de venir raconter une histoire et changer de frein.
6. Debriefer ensemble de comment chacun·e s'est senti·e, lorsqu'iel racontait / écoutait. Voir si certain frein sont plus habituels, agréables que d'autres.

# liste des freins à l'écoute empathique

- moraliser
- clore la question
- surenchérir
- minimiser
- dévier sur soi
- culpabiliser
- victimiser
- expliquer
- consoler
- conseiller
- interroger
- corriger
