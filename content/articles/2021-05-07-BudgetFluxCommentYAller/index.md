---
date: 2021-05-07
title: "La gestion budgétaire en mode flux : Comment y aller ?"
lang: fr
authors:
  - retiere_samuel
tags:
  - budget
draft : false
categories:
  - organisation
illustration:
  name: 8244072348_bf80531c52_c.jpg
---

Expliquer comment passer d'une approche budgétaire traditionnelle à une approche en mode flux / produit

<iframe width="100%" height="410" sandbox="allow-same-origin allow-scripts allow-popups" src="https://video.ploud.fr/videos/embed/cc67da4d-430b-47a1-a098-6abe0c708f9c" frameborder="0" allowfullscreen></iframe>


1. Un processus budgétaire continu, comment y aller ?
2. La dernière fois Je vous ai parlé de deux façons de voir le budget Mais je n’ai pas expliqué comment passer de l’un à l’autre La traditionnelle Le mode flux 
3. Ou comment passer de ça Tout est alloué dès le début On détermine le prochain budget Budget année + 1 = Budget année +/- x% Tout est alloué à la maille projet. On vérifie après coup la répartition par ligne métier. En fonction des projets priorisés, on ajuste la taille des équipes. L’enveloppe Les projets Les équipes
4. x 200 A ça Des équipes stables, une allocation au fil de l’eau Budget glissant sur 5 trimestres Allocation stratégique Les équipes Ecosystème A Ecosystème C Ecosystème B Allocation dynamique x 80 x 50 Durée < 3 mois Au moins une livraison Rolling forecasts
5. Mais je n’ai pas expliqué comment y aller ? 
6. Voici notre démarche en 13 étapes Sachant que nous allons plus jouer … au Go … … qu’au échec Depuis le temps, il n’y a pas d’urgence
7. C’est quoi le problème ?
8. Que voulons nous changer ? La description d’un présent futur Etre capable de répondre plus vite
9. Nous créons les conditions favorables au changement En figeant les positions de départ x 200 x 80 x 50
10. Nous découpons les projets Pas plus de 3 mois sans livrer en production ET mesure de valeur
11. Inscrivons nous dans la gouvernance actuelle Au début, ne changeons rien Sauf la valeur 
12. Les chiffres Puis, nous adaptons la gouvernance Nous coupons les projets qui ne livrent pas de valeur
13. Et enfin, nous coupons le courant Tout budget non commencé est mis au pot commun
14. Commençons l’allocation dynamique 
    * Fréquence mensuelle 
    * Gestion des dépendances entre écosystèmes 
    * Affiner les roadmaps 
    * Fréquence mensuelle 
    * « Projets » triés en fonction du tri initiative 
    * Nous donnons (dans l’ordre) des projets aux équipes 
    * Fréquence trimestrielle 
    * Explicitation des axes stratégiques 
    * Tri des initiatives Prio / Planif long terme Priorisation court terme Planification long terme
15. Nous trouvons des poches cachées Il est fini ce projet ? Consciemment ou pas En fait, c’est pas un vraiment un projet, c’est plus une enveloppe d’évolutions C’est quoi les trois projets qui ont le même nom ? Ce projet, c’est le cadrage de tel autre C’est normal d’avoir du consommé par TBD ? C’est un budget pluriannuel, on ne peut l’arrêter Tu vois, c’est un forfait de fin d’année pour dépenser le budget
16. Le tri unique des initiatives 1 est avant 2 2 est avant 3 3 est avant 4 4 est avant 5 5 est avant 6 … Tu prends le premier que tu peux prendre Keep It Simple & Stupid ou soyons bêtes et cons
17. Nous découvrons des contraintes 
    * Bénéfice AVANT intérêts, impôts et amortissements 
    * Dépenses d’exploitation 
    * Charges courantes pour exploiter un produit, une entreprise, ou un système 
    * Dépenses d'investissement 
    * Immobilisations, dépenses qui ont une valeur positive sur le long terme. Capex Opex Ebitda 
18. Prenons en compte les retours C’est parfois là que ça fait mal. Et adaptons la stratégie
19. Commencer par le qui Nous allons ouvrir, sans nécessairement le savoir, des sujets que tout le monde n’a pas envie de voir. Et nous allons donner de l’autonomie Alors que c’est plus simple de faire comme tout le monde. 
20. Mon conseil de fin Prenez votre temps Mais, ne vous arrêtez pas
21. Merci
