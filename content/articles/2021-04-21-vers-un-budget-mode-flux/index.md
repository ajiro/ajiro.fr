---
date: 2021-04-21
title: "Comprendre la gestion budgétaire en mode flux"
lang: fr
authors:
  - retiere_samuel
tags:
  - budget
draft : false
categories:
  - organisation
illustration:
  name: villarceau.jpg
---

Expliquer la différence entre les approches budgétaire traditionnelle et en mode flux

<iframe width="100%" height="410" sandbox="allow-same-origin allow-scripts allow-popups" src="https://video.ploud.fr/videos/embed/129e5a33-8e46-475d-b2f7-ed653b5003c3" frameborder="0" allowfullscreen></iframe>

1. Vers un processus budgétaire continu
2. 2 Le budget : A quoi ça sert ? Une direction commune pour l’organisation. Tout le monde connait sa place dans la vision globale. Définir des objectifs Suivre l’avancement et ajuster en cas de besoin. Les données sont une aide à la décision. Suivre un prévisionnel Allouer ses ressources rares là où cela fait le plus de sens pour l’entreprise. Se donner la capacité de se réorienter en cas de besoin. Allouer les ressources
3. 3 ➢ Les initiatives en lien direct avec la stratégie ➢ La colonne vertébrale de l’organisation. ➢ La maintenance évolutive ➢ Une portion du budget qui peut être décentralisée ➢ C’est le désordre de l’organisation Les 3 types d’investissement ➢ Maintien en conditions opérationnelles ➢ Support de production & maintenance corrective ➢ Monitoring & Incident management Le support de production Les petites demandes Les changements majeurs Garder la lumière allumée L’huile du système Les grands projets
4. 4 A quoi ça ressemble ? L’approche traditionnelle
5. 5 Années 1920 Un peu d’histoire Le processus de budgétisation est utilisé par les entreprises pour contrôler la demande des consommateurs, ,ainsi qu’adapter organisation et stratégie. 
6. 6 La definition des objectifs La lettre au père noel Nous décidons Automne Nous affinons Hiver  Nous délivrons Printemps Nous délivrons Eté Stratégie à ? années
7. 7 L’allocation de ressources Tout est alloué dès le début On détermine le prochain budget Budget année + 1 = Budget année +/- x% Tout est alloué à la maille projet. On vérifie après coup la répartition par ligne métier. En fonction des projets priorisés, on ajuste la taille des équipes. L’enveloppe Les projets Les équipes
8. 8 Le prévisionnel Suivi et arbitrage Nous suivons les projets en contrôlant l’écart à la normale Consommé vs Budget prévisionnel
9. 9 Prérequis La cible doit être stable, votre fréquence de décision étant annuelle. Objectifs Votre structure organisationnelle bouge peu. Allocation de ressources Le lien entre le livrable et le résultat est clair et équivoque. Prévisionnel Vous n’avez qu’un slot pour prendre des décisions majeures. Donner, c’est donner. Reprendre, c’est voler. Un budget alloué revient rarement. Focus sur les livrables plus que l’impact 
10. 10 A quoi ça ressemble ? L’approche flux
11. 11 ➢ Les initiatives en lien direct avec la stratégie ➢ La colonne vertébrale de l’organisation. ➢ La maintenance évolutive ➢ Une portion du budget qui peut être décentralisée ➢ C’est le désordre de l’organisation Les 3 types d’investissement (rappel) ➢ Maintien en conditions opérationnelles ➢ Support de production & maintenance corrective ➢ Monitoring & Incident management Le support de production Les petites demandes Les changements majeurs Garder la lumière allumée L’huile du système Les grands projets
12. 12 La définition des objectifs Relier stratégie et exécution • Une cible est décrite comme un changement par rapport à aujourd’hui. • Les axes stratégiques représentent les thèmes majeurs de cette intention. • Pour chaque axe, des objectifs long terme sont décrits et déclinés sous la forme de résultats observables court terme. • Les projets peuvent subsister, ils sont cependant découpés en lot de valeur (et pas découpage technique) Objectifs de 9 à 18 mois You are here T1 KR T2 KR T3 KR La cible Autonomie Aujourd’hui No way X axes stratégiques Coeur
13. 13 Objective Key Results alias OKR Un exemple pour garder le lien entre stratégie et exécution
14. 14 x 200 L’allocation de ressources Des équipes stables, une allocation au fil de l’eau Budget glissant sur 5 trimestres Allocation stratégique Les équipes Ecosystème A Ecosystème C Ecosystème B Allocation dynamique x 80 x 50 Durée < 3 mois Au moins une livraison Rolling forecasts
15. 15 Allocation dynamique Un exemple de gouvernance JAN FEV MAR APR MAY JUNE JULY AUG SEPT OCT NOV DEC Priorisation court terme Planification court terme Priorisation moyen terme & allocation de ressources 5 trimestres 1 trimestre 1 trimestre
16. 16 Le prévisionnel Suivi et arbitrage Nous suivons la valeur délivrée Pour décider: ✓ Continuer ✓ Réorienter ✓ Arrêter
17. 17 Prérequis Donner de la liberté sur les décisions tactiques ET suivre les décisions stratégiques. Objectifs Changer rapidement de directions veut dire ressources polyvalentes. Allocation de ressources Des boucles de feedback courtes basées sur les résultats (et non des livrables). Prévisionnel Une exécution disciplinée et le respect des niveaux de décisions sous peine de gripper le système. Investir sur les personnes sous peine d’avoir des personnes incompétentes. Si votre time to market est supérieur à 6 mois, travaillez d’abord sur ce sujet.
18. 18 Nous préférons Surfer la vague … … plus que la subir Dans un monde où il y a beaucoup d’incertitude, les entreprises les plus performantes peuvent changer rapidement de direction. Vous ne pouvez pas arrêter l’horloge et le business as usual est un concept révolu.
19. Merci
