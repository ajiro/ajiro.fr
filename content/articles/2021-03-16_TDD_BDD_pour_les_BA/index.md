---
date: 2021-03-16
title: "BDD & TDD pour les non-techniques"
lang: fr
authors:
  - clavier_thomas
tags:
  - BDD
  - TDD
categories:
  - craft
illustration:
  name: code.jpg
---

Expliquer l'intrication entre TDD et BDD à travers un livecoding. Voilà le challenge que j'essaie de relever dans cette vidéo.

<iframe width="100%" height="410" sandbox="allow-same-origin allow-scripts allow-popups" src="https://video.ploud.fr/videos/embed/23357aee-97a0-490e-b5e4-a35336a987f7" frameborder="0" allowfullscreen></iframe>
