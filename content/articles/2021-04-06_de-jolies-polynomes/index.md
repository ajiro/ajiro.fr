---
date: 2021-04-06
title: "De jolies polynômes"
lang: fr
authors:
  - clavier_thomas
tags:
  - TDD
  - live coding
  - kata
categories:
  - craft
illustration:
  name: polynomial-pretty-print.jpg
---

Dans ce kata relativement court, je résous un petit défi lancé par un collègue de math : joliment afficher des polynômes de degrés entiers.

<iframe width="100%" height="410" sandbox="allow-same-origin allow-scripts allow-popups" src="https://video.ploud.fr/videos/embed/d858b7fe-8f6a-48c1-8d64-811f7c2a9d87" frameborder="0" allowfullscreen></iframe>

Suite à ce livecoding, j'ai ajouté le sujet sur [coding dojo](https://codingdojo.org/fr/kata/polynomial-pretty-print/).
