---
date: 2022-06-01
title: "De la prospective au service de notre réactivité"
lang: fr
authors:
  - retiere_samuel
tags:
  - budget
draft : false
categories:
  - organisation
illustration:
  name: nos-futurs.png
---

## Version courte

Ça va être dur vu que c’est le déroulé d’un atelier pour aider à se projeter vers l’avenir, l’idée n’étant pas de faire des prédictions pour qu’elles soient justes, mais de faire des projections qui nous aident à comprendre comment le monde change. Au passage, c’est aussi un exercice d’alignement.

## Version longue

À la base, quand une personne parle de prospective, elle parle souvent d’un exercice de projection très long terme comme « La ville dans 30 ans ». Si j’ai toujours trouvé intéressant de se projeter un peu pour être capable d’influer sur le futur, j’ai aussi trouvé que la réplication d’une pratique utilisée pour des projections à 30 ans avait ses limites. C’est dans cette idée que je les ai adaptés pour des projections plus court terme. Aujourd’hui, je m’en sers pour faire des projections d’une à quelques années. L’intérêt ne réside pas tant dans la précision de la projection, que dans notre capacité à comprendre plus vite le monde qui change et donc à se réorienter plus vite. Dans la pratique, je conseille sur un même sujet de le faire tous les 6 mois.

Histoire de donner un peu de perspective sur le déroulé de l'atelier, voici une petite explication de texte sur les futurs, librement inspirés de Jabe Bloom. Nous avons donc les futurs suivants :
- Le probable : Le futur est vu comme une suite linéaire du présent. Il s'appelle probable car nous considérons qu'entre deux événements possibles, c'est le plus probable qui se passera.
- Le possible : Nous prenons le contrepied du futur possible. Il s'agit d'envisager des événements improbables sans pour autant tomber dans la science fiction. Nous étirons notre pensée en nous projetant sur des scénarios extrèmes.
- Le plausible : Si la probabilité qu'un événement improbable donné arrive est faible, la probabilité qu'un événement improbable survienne est beaucoup plus forte. Nous ne savons pas encore lequel, mais il y a aura des points d'inflexion. Le plausible est à mi chemin entre le probable et le possible.
- Le préférable : Maintenant que nous avons envisagé différents futurs possibles, il s'agit de nous projeter à l'intérieur et de décrire où nous souhaiterions être. Il ne restera alors plus qu'à nous demander comment influencer le présent pour aller vers ce futur préférable.

### Round 0 

Choisir un horizon de temps et un thème. Par exemple, nous allons travailler ensemble sur l’avenir de notre plateforme technique à 2 ans. L’idée n’est pas à la fin de faire un plan d’action précis entre aujourd’hui et notre cible, mais bien de décider des grandes orientations et d’avoir envisager différents scénarios. Pour la suite, c’est un atelier collaboratif qui nécessite de regrouper les personnes intéressées par le sujet.

Pour une démo du workshop en une heure, on part sur une cible à 18 mois avec comme un thème générique comme « L’avenir de la tech », et des rounds de 10 minutes (3 minutes écriture post it, 3 minutes de partage, 3 minutes de débrief et 1 minute de marge). Pour être confortable sur un vrai sujet, on monte à 20 minutes par round (5 minutes d’écriture, 5 minutes de partage, 5 minutes pour critiquer/affiner, 5 minutes de débrief), ce qui nécessite au moins 2 heures. Sur le round 5, l’option à base de démocratie profonde prend 4 heures (1h30 démocratie profonde, 30 minutes de vote, 1 heure pour décrire par groupe les expérimentations d’influence). Sur l’option 5 version stratégie, on part tout de suite sur la journée.

### Round 1: Les futurs probables

Décrire des comportements visibles à la cible si le futur n’est que la suite du passé. Les tendances se poursuivent et il n’y a pas de point d’inflexion. Nous traçons le trait droit. Nous sommes toujours du bon côté des statistiques.

### Round 1 bis : 

Comment le savez-vous ? Qu’est-ce qui vous permet de dire ça ? Cette étape peut être faite de façon indépendante ou option préférée en demandant à l’oral de préciser quand quelqu’un décrit un post it, ce qui lui a fait écrire cela.

### Round 2 : Les signaux faibles

Qu’est ce que vous avez vu qui vous fait dire que le futur ne sera pas juste la suite du présent ? Il y a les grands messages qui portent partout et que tout le monde connait comme « Le chef a dit ça au dernier grand amphi » et d’autres messages que vous percevez comme signe avant-coureur d’une inflexion comme « Aujourd’hui, j’étais à côté d’un gars qui bossait sur un AS400 dans le train ».

### Round 3 : Les futurs possibles

L’idée, c’est de taper le bord du cadre. Des événements improbables se passent avec des points d’inflexion entre passé et présent. Que voyons-nous ? Qu’est ce qui nous a mené là ? Pour rappel, nous décrivons des comportements visibles si nous sommes téléportés dans cette cible.

### Round 3 bis : 

On regroupe par thème puis : Comment le saurez-vous (vous dans votre organisation) ? Que ferez-vous si cela survient ?

### Round 4 : Les futurs plausibles

Nous resserrons les futurs possibles en nous inspirant des futurs probables. Quels sont les comportements visibles qui vous paraissent faire du sens. Quelques événements improbables surviennent avec quelques révolutions (d’autant plus si nous sommes français). Que voyez-vous comme comportements visibles.

### Round 4 bis : 

On regroupe par thème puis : Comment le saurez-vous ? Que ferez-vous si cela survient ?

## Versions alternatives pour conclure

### Round 5 version courte : On conclut en s’arrêtant là. 


Le but n’est pas de faire un plan d’action, mais de vous aider à mieux comprendre que le monde change. Cela augmentera d’autant votre capacité de réaction. Parce que vous avez réfléchi à ce qui pourrait se passer, vous serez d’autant plus réactif si un scénario commence à prendre forme. Vous pourrez aussi invalider des scénarios en constatant l’absence de signaux. Si ce scénario était vrai, vous auriez du voir ça, hors ça n’est pas ce que vous constatez. C’est une non information qui vous aide à comprendre que le monde change.

### Round 5 version début de convergence : Le futur préféré

Vers où souhaitez-vous aller en tant qu’organisation ? Comment souhaitez-vous influencer le futur pour qu’il aille dans la direction que vous voulez ? C’est le moment de la minute théorique. Nous sommes fragiles si un événement improbable nous paralyse. Nous sommes résilients si nos mesures de mitigation des risques nous permettent de passer une crise. Nous sommes anti fragile si nous profitons des opportunités qu’apportent un événement improbable. Nous sommes plus forts après qu’avant.

### Round 5 bis : Comment saurez-vous que vous réussissez à aller dans cette direction ?

### Round 5 version nous influençons le monde : Les futurs préférés

Le round 5 peut être fait sous la forme d’un atelier « Démocratie profonde » pour entendre toutes les voies du système, couplé à un vote (consensus systémique puis majorité) et pour finir une étape de convergence en formalisant les actions sous la forme d’un modèle d’expérimentation (postulats, hypothèses, actions, critères de validation). Cette option s’appelle « Nous influençons le monde » car vous allez essayer par quelques d’actions de faire que le monde aille dans la direction que vous voulez.

### Round 5 version définir une stratégie : De la stratégie à l’exécution

Grosso modo, on fait un double diamant divergence/convergence sur la vision (futur préféré). On commence par clarifier les problèmes que l’on veut résoudre, puis on définit les solutions à mettre en face. Nous avons décrit la vision sous forme de problèmes que nous souhaitons résoudre et de comportements visibles. Nous regroupons ensuite par thème qui vont devenir nos axes stratégiques. Il reste ensuite à décliner sous la forme d’objectifs. Cela mérite un modus operandi dédié.

Nous arrivons à la fin de cet article et il ne me reste plus qu’à conclure : Essayez-le et vous verrez bien. Ça ne prend pas tant de temps que ça.

