---
date: 2020-10-06
title: "Langage statique : l'éviter ? Mais pourquoi et comment ?"
lang: fr
draft: false
authors:
  - quille_julie
tags:
  - CNV
  - langage dynamique
  - CR discussion de groupe
categories:
  - CNV
illustration:
  name: dynamique.jpg
  source: https://pixabay.com/fr/users/geralt-9301/
description: |
  Une des questions de la Communication NonViolente (CNV) est de savoir ce qui est vivant dans le moment présent. Marshall parle également de langage dynamique, qu'il met en perspective par rapport à un langage statique. Aujourd'hui, nous avons pris un temps à quatre, pour échanger sur où nous en étions dans notre compréhension de ces concepts.
---

Une des questions de la Communication NonViolente (CNV) est de savoir ce qui est vivant dans le moment présent. Marshall parle également de langage dynamique, qu'il met en perspective par rapport à un langage statique. Aujourd'hui, nous avons pris un temps à quatre, pour échanger sur où nous en étions dans notre compréhension de ces concepts, notamment vis-à-vis de valeurs qui peuvent sembler statiques, du lien que nous souhaitons prioriser, de l'interdépendance entre l'être et le faire, et enfin le choix de vivre le dynamisme en tout temps à travers l'écoute.
Nous vous proposons ici un compte rendu de nos réflexions. Ce dernier est vraiment destiné aux personnes pratiquant déjà la CNV depuis un moment et cherchant à débattre de certaines idées clés, nous ferons d'autres publications pour les personnes souhaitant commencer la CNV ou vivre davantage de pratique.

# Peut-on exprimer une croyance en langage dynamique ?

**À mon avis, nous devrions éliminer de notre répertoire les mots suivants : juste, faux, bon, mauvais, normal, anormal, compétent, incompétent. Ces mots font partie d'un langage statique. [...] À l'opposé de cela, la CNV est une langue dynamique, décrivant la vie en mouvement.**

  *Marshall Rosemberg, Dénouer les conflits par la Communication NonViolente, édition Jouvence*

Au premier abord, nous pourrions penser qu'il est difficile d'avoir une croyance ou d'exprimer une valeur qui nous anime tout en utilisant un langage dynamique. Par exemple comment enlever "mauvais" de la croyance "Les violences physiques sur les enfants sont mauvaises" ? Ou encore, nous pouvons nous questionner sur comment vivre pleinement l'invitation de Marshall de préserver nos croyances et valeurs soutenantes, et en même temps de favoriser un langage dynamique. Enfin, il est possible qu'une partie de nous se soulève en criant au relativisme absolu !
La première chose qui nous vient en tête est de nous dire qu'il y a un temps pour toute chose et que la Communication NonViolente nous invite avant tout à regarder ce qui est vivant à chaque instant, puisse cela être une croyance exprimée en langage statique.
Et pour autant nous aimerions également partager qu'aujourd'hui, nous avons la conviction que stabilité n'est pas équivalent à staticité, réciproquement, dynamisme ne veut pas dire absence de constance.
Ainsi d'un côté nous pourrions avoir une suite de vérités à chaque instant, qui peuvent se ressembler sans être tout à fait identiques.
Pour reprendre l'exemple que nous avions au départ :
* À un instant je peux être particulièrement en contact avec le fait que je trouve néfaste les violences faites aux enfants.
* À un autre instant je peux ne pas y penser du tout et cela est absent de mon esprit.
* Enfin je peux me retrouver dans une dispute avec mon fils ou ma fille et sentir une grande envie d'utiliser la force physique pour me faire entendre.

C'est en faisant la moyenne de tous ces points que j'arrive à en déduire une constante que je peux avoir tendance à exprimer sous forme d'une généralité avec un langage statique : "Les violences faites aux enfants sont mauvaises".

> **C'est en faisant la moyenne de tous ces points que j'arrive à en déduire une constante.**

De notre point de vue, la CNV ne nous invite pas à revoir systématiquement ces constances mais plutôt à apprendre à faire un zoom à l'endroit où nous sommes et exprimer ce qui est vivant à l'instant t :

Par exemple :
* "Quand je vois des parents crier sur leur enfant dans la rue, je me sens particulièrement en contact avec le fait que les violences faites aux enfants me font horreur"

Une invitation est donc de venir mettre de l'espace dans ces endroits qui peuvent se figer pour y remettre du mouvement et donc de la vie.

```

À vous de jouer :
Traduisez en langage dynamique les phrases suivantes :
  - Les femmes sont plus sensibles que les hommes.
  - Mon enfant est introverti.
  - Je suis une personne curieuse.
  - Il est préférable de dire ce que l'on pense, plutôt que de faire semblant.
  - Je déteste l'odeur du vin.

```

# Impact du faire sur l'être et de l'être sur le faire

À ce stade, vous vous dites peut-être : "**que de charabia pour si peu de différence !**". En effet, il est aisé de prendre des raccourcis dans la façon dont on s'exprime, que ce soit pour gagner en clarté, en efficacité ou toute autre raison. Cela ne veut pas dire pour autant que nous ne sommes pas conscients de la subjectivité des propos. Nous pouvons être convaincu·e·s qu'il n'y a pas forcément corrélation entre la façon dont nous pensons et la façon dont nous nous exprimons et vice versa.
Néanmoins nous avons pu constater qu'il arrive régulièrement que la répétition de simplifications peut avoir un impact sur la façon dont nous vivons les choses. Dans le cadre de l'éducation, nous sommes aujourd'hui beaucoup plus attentif·ve·s aux étiquettes qu'il y a quelques années. Vous savez les étiquettes, ces petites simplifications anodines qui nous font nous adresser à autrui avec un "Tu es...", un "les femmes sont...", "les petits garçons aiment..." etc.. Malheureusement, nous voyons aujourd'hui que les enfants que nous étiquetons "Tu es maladroit", "Tu es bête" ou encore "Tu es sage", "Tu es fantastique" ont tendance à internaliser ces adjectifs et à avoir rapidement des comportements qui viennent confirmer le pronostic (voir entre autre l'[effet Pygmalion](https://fr.wikipedia.org/wiki/Effet_Pygmalion) ) ou encore, convaincu qu'il est bête, l'enfant n'essayera même pas d'entreprendre certaines activités qui lui font pourtant envie.

Nous avons la croyance, qu'adultes, nous fonctionnons un peu de la même manière, à force de nous exprimer en langage statique (jugements, généralités, préjugés etc.), nous rentrons dans un monde construit sur une réalité figée dans laquelle nous perdons tout pouvoir d'agir et de prendre du recul, là où la CNV nous invite à nous connecter au mouvement de la vie et à un espace de choix.

Nous sommes également persuadé·e·s que si nous mettons de l'énergie à aller vérifier régulièrement comment nous nous sentons, ce qui est vrai pour nous dans l'ici et maintenant, cela modifiera petit à petit notre manière de voir le monde. Grandie de cette nouvelle capacité à ouvrir les possibles, comprendre la divergence des réalités, notre manière de nous exprimer évoluera elle aussi dans une direction qui permette d'exprimer avec encore plus de précision notre état présent.

```

À vous de jouer :
Tester de lire les deux types de phrases suivantes et voyez si vous sentez une différence ou non :

Je suis nul·le.

VS

Quand je renverse mon café sur mes documents, je me sens agacée, parce que j'aurai aimé être plus attentif·ve et garder mes documents propres.

Je sus génial·e.

VS

Je suis heureuse de voir que mes collaborateurs sont enthousiastes par le travail que j'ai fait.

Mon voisin est un con.

VS

Quand mon voisin laisse son chien faire ses besoins au milieu du trottoir, je me sens très irrité et aimerais vraiment qu'il y ait une attention collective sur la propreté de la rue.

```

# Phrase à rallonge et vocabulaire inaccessible

Il est vrai que si nous prenons le temps d'émettre l'intégralité des subtilités que nous vivons dans chacun de nos instants, nous pouvons y perdre de notre naturel et cela peut même nuire à la qualité de la relation.

Tout d'abord, nous voyons chacune des invitations à modifier notre façon de nous exprimer en utilisant des structures bien précises comme des opportunités de faire nos gammes. En effet, de la même manière qu'il n'existe, à notre connaissance, aucun individu qui, la première fois qu'il s'assied devant un piano, joue de manière exquise un morceau de Mozart, aucun·e d'entre nous ne peut, simplement parce qu'iel l'a décidé, devenir un as du langage dynamique en un claquement de doigts. Mais de la même manière que petit à petit le pianiste va pouvoir exprimer sa personnalité à travers la musique et se libérer des partitions, nous apprenons petit à petit à nous approprier ce langage pour qu'il soit au service de ce que nous souhaitons exprimer.

Néanmoins, comme nous l'avons dit plus haut : chaque chose en son temps. Nous vous invitons à voir le langage dynamique comme une couleur supplémentaire que nous choisissons d'ajouter à notre palette, un choix de plus. Ainsi à chaque instant nous sommes invité·e·s à choisir ce que nous souhaitons prioriser (la simplicité, l’entraînement, le confort, la justesse etc.), et il arrive aussi que parfois, nous passions en mode automatique, sans même nous en rendre compte... et ça aussi, ça fait partie intégrante du vivant :-D .

Enfin, nous pouvons aussi mettre notre attention sur notre intention. De la même manière qu'il existe des spécialistes de musique, qui pourront décoder n'importe quel morceau avec finesse, alors qu'ils ne savent pas aligner deux notes sur un instrument, nous pouvons ne rien changer à nos habitudes dans l'expression tout en mettant de l'énergie à être attentifs à ce qui se passe en nous lorsque nous le disons ou encore à voir qu'à travers une expression que nous pourrions assimiler à une vérité générale, il n'y a que l'expression d'une réalité vécue à un instant donné par une personne donnée. Cet exercice de pensée nous amène directement au dernier point que nous souhaitions aborder ici.


# L'écoute : lieu de tous les possibles

Lorsque nous parlons de langage, nous avons souvent tendance à penser d'abord à la façon dont nous nous exprimons. Or le langage c'est au moins tout autant choisir comment nous écoutons que comment nous nous exprimons.
Lorsque nous nous exprimons nous pouvons prendre le temps de vérifier : ce qui a été compris, et comment la personne se sent par rapport à cela. Néanmoins, nous sommes encore très dépendant·e·s des représentations majoritaires. 

Par exemple :

  - votre ami arrive à 15h30 alors que vous aviez RDV à 15h, vous sentez que cela vous agace parce que vous auriez aimé profiter de ce temps pour faire quelques courses et en même temps vous savez très bien que si votre ami est arrivé plus tard que prévu c'est qu'il a fait quelque chose d'important pour lui.

Comment essayeriez-vous de le lui communiquer ? Pouvez-vous vous rappeler de situation où vous avez vécu quelque chose de similaire et que vous n'avez pas osé dire par peur que la personne l'entende comme un reproche ?
Réciproquement, vous arrive-t-il de vous en vouloir lorsque quelqu'un vous partage que vos actions ont eu un impact différent de ce que vous souhaitiez (faire plaisir, aider, prendre soin de vous etc.) alors qu'on prend soin de vous rassurer sur l'impact en question? 

Et bien la bonne nouvelle, c'est que nous avons chacun toute la liberté d'entendre la vérité de l'autre en langage dynamique :

* en l'accueillant telle qu'elle est,
* en acceptant que nous pouvons avoir une réalité différente,
* en nous rappelant que la mesure se fait dans la longueur et que chaque partage est une opportunité de construire un peu plus la relation dont on rêve (à soi et aux autres),
* en apprenant à traduire à l'intérieur de nous ce que nous entendons pour être en contact avec la vérité ponctuelle qui est vécue à un instant donné.

Ce dernier point nous semble particulièrement joyeux dans la mesure où nous pouvons décider seul·e  de la manière dont nous écoutons. C'est une façon de s’entraîner qui nous est toujours accessible. Par ailleurs, nous pouvons constater la vitesse à laquelle nous nous racontons des histoires dans notre tête lorsque nous entendons des jugements qui ne sont pas en accord avec nos valeurs. L'invitation est d'allonger le temps qui sépare le moment où l'on entend une phrase et celui où nous réagissons. Et puisqu'il s'agit d'une expérimentation que nous pouvons pratiquer tout au long de notre vie, il ne nous reste plus qu'à vous souhaitez une bonne pratique :-D


# Gratitude à :

> * Diane : qui nous rappelle l'importance de l'intention, dans la notion de choix de faire ou ne pas faire, ainsi que pour l'énergie qu'elle met pour que l'accessibilité reste au cœur des échanges.
> * Guillaume : qui nous rappelle que la vie est mouvement, et que le résultat se mesure dans la durée, chaque instant n'étant pas forcément représentatif de la moyenne.
> * Julie : qui nous rappelle que parfois nous sommes à un endroit où nous ne sommes plus en mesure de prendre du recul. Le langage dynamique est également d'accepter ces instants.
> * Romeu : qui remet au cœur des échanges l'engagement sociétal et qui reste garant des subtilités existantes entre stabilité / staticité et dynamisme (entre autre).

> * **Et à chacun·e d'avoir mis son cœur et son énergie dans ces échanges au service de l'épanouissement.**

# Pour la suite :
    
Nous n'avons pas pris tout le temps que nous aurions aimé sur les sujets suivants :

* l'engagement social : responsabilité collective et responsabilité individuelle
* la CNV et le langage courant : comment être accessible et créer des passerelles plutôt que des fossés
* importance de la disponibilité intérieure et besoin de soutien extérieur
* prendre la responsabilité de la façon dont nous choisissons de voir les choses
