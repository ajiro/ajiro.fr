---
date: 2019-07-30
title: "Behavior driven development"
lang: fr
draft: false
authors:
  - clavier_thomas
tags:
  - agile
  - BDD
  - gherkin
categories:
  - craft
illustration:
  name: bdd.jpg
  source: /authors/clavier_thomas/
description: |
  Si je devais faire une conférence sur le BDD, je parlerais de quoi ?
---

Il y a quelque temps, j'avais dans l'idée de construire une affiche sur le Behavior Driven Development (BDD), un mémo qui reprendrait tout ce que nous racontons sur le sujet aux équipes. Dans cette optique, j'ai profité d'une soirée de travail avec une bonne partie de l'équipe pour lancer le sujet. La question que j'ai posée était la suivante : Si vous deviez faire une conférence sur le BDD de quoi parleriez-vous ?

Nous avons ainsi construit le fil conducteur d'une conférence. L'illustration de cet article est ma synthèse sous forme de sketchnote de cette histoire. Vous pouvez la télécharger [ici](./bdd.jpg)

Le point de départ, c'est l'échange entre les 3 amis avec l'objectif de se comprendre pour réduire le "re-work" et guider la phase de développement.
La perte de temps liée au manque de compréhension entre développeur et business est considérable.
Il n'est pas rare de devoir refaire plusieurs fois la même chose, de découvrir à postériori qu'une fonctionnalité n'était pas indispensable ou qu'à ce prix ça ne valait pas le coup de la faire.

Seconde étape, c'est la création d'un langage commun : "l'Ubiquitous Language" du Domain Driven Design (DDD). Ce langage qui va permettre de décrire avec les mêmes mots à la fois le code et les exemples.·

Troisième étape, c'est l'usage d'outils complémentaires pour minimiser les incompréhensions.
Les exemples et les personas vont lever un certain nombre d'ambiguïtés. Les personas apportent un autre élément important : la compréhension du pourquoi l'utilisateur va faire l'action.

Enfin, dernier point, c'est le langage Gherkin qui permet de décrire les histoires utilisateurs et les exemples de façon structurée. Cloue du spectacle, l'automatisation du contrôle de ces règles.

Petit aparté avant de terminer avec un zoom sur la structure d'un bon exemple. Il est structuré autour des étapes "Given", "When" et "Then". "Given" expose le contexte d'entrée et permet de décrire le système avant le test avec un ensemble d'éléments observables. "When" correspond à l'action effectuée par l'utilisateur. Enfin, le "Then" permet de décrire l'état attendu avec là encore des éléments concrets et observables.

En guise de conclusion, je terminerai sur l'importance de ne pas confondre UI (Interface Utilisateur) et UX (Expérience Utilisateur), sachant que Le BDD décrit exclusivement l'expérience utilisateur et non les interfaces.
