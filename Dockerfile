FROM registry.gitlab.com/tclavier/docker-nginx

RUN apt-get update \
 && apt-get install -y \
    make \
    wget \
 && apt-get clean

RUN wget https://github.com/gohugoio/hugo/releases/download/v0.101.0/hugo_extended_0.101.0_Linux-64bit.deb -O /tmp/hugo.deb \
 && dpkg -i /tmp/hugo.deb \
 && rm -f /tmp/hugo.deb

RUN mkdir -p /etc/letsencrypt/live/ajiro.fr/ \
 && openssl req -x509 -nodes -days 365 -newkey rsa:4096 \
            -keyout /etc/letsencrypt/live/ajiro.fr/privkey.pem \
            -out /etc/letsencrypt/live/ajiro.fr/fullchain.pem \
            -subj '/CN=ajiro'

COPY . /site
WORKDIR /site

RUN hugo_env=production hugo --destination=/var/www/prod
RUN hugo --buildDrafts --buildFuture --destination=/var/www/draft \
 && echo "User-agent: *" > /var/www/draft/robots.txt \
 && echo "Disallow:/ "  >> /var/www/draft/robots.txt

COPY nginx_vhost.conf /etc/nginx/conf.d/ajiro.conf
