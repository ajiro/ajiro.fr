IMAGE = ajiro

#$(patsubst %.png, %__thumbnail-wide.png, $(PNG_ORIGINAL))


all: hugo

download_images:
	./analyse.py images download

build:
	docker build --pull -t ${IMAGE} .

daemon: build
	@docker rm -f ajiro || true
	@docker network create --driver bridge ajiro-fr || true
	docker run --name ajiro -d -p 8081:443 --network ajiro-fr ${IMAGE}

linkchecker: daemon
	docker run --network ajiro-fr -it registry.gitlab.com/azae/docker/linkchecker linkchecker --check-extern --anchors --ignore-url=^mailto: https://ajiro/

a11y: daemon
	docker run --network ajiro-fr -it registry.gitlab.com/azae/docker/pa11y pa11y-ci --sitemap https://ajiro/sitemap.xml --config pa11y.conf


test: linkchecker a11y

hugo:
	hugo server --buildDrafts --buildFuture --watch --bind 0.0.0.0 --port 8000

docker-dev: build
	@docker rm -f ajiro || true
	@docker network create --driver bridge ajiro-fr || true
	docker run --name ajiro -p 8000:8000 --network ajiro-fr --rm -it -v $(shell pwd):/site --entrypoint=/bin/bash ${IMAGE}
